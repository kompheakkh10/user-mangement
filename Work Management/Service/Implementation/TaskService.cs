﻿using Microsoft.EntityFrameworkCore;
using Work_Management.Entity;
using Work_Management.Request;
using Work_Management.Response;
using WorkManagement.Services;

namespace Work_Management.Service.Implementation;

public class TaskService : ITaskService
{
    private readonly SqlDBContext _context;

    public TaskService(SqlDBContext context)
    {
        _context = context;
    }

    public IEnumerable<TaskResponse> GetTasks()
    {
        IEnumerable<Tasks> tasks = _context.task.Include(t => t.user).ToList();
        return MapTasksToTaskResponses(tasks);
    }

    private IEnumerable<TaskResponse> MapTasksToTaskResponses(IEnumerable<Tasks> tasks)
    {
        return tasks.Select(task => new TaskResponse
        {
            TaskId = task.TaskId,
            TaskName = task.TaskName,
            Status = task.status,
            UserId = task.UserId,
            User = new UserResponse
            {
                Username = task.user?.Username,
            }
        });
    }   

    public TaskResponse GetTaskById(int taskId)
    {
        Tasks task = _context.task.Include(t => t.user).FirstOrDefault(t => t.TaskId == taskId);

        if (task == null)
        {
            return null; // or handle as needed
        }

        return MapTaskToTaskResponse(task);
    }

    public TaskResponse AssignTask(TaskRequest taskRequest)
    {
        Users user = _context.user.Find(taskRequest.UserId) ??
                     throw new ArgumentNullException("_context.user.Find(taskRequest.UserId)");

        Tasks task = new Tasks
        {
            user = user,
            TaskName = taskRequest.TaskName,
            UserId = taskRequest.UserId,
            status = taskRequest.Status
        };

        _context.task.Add(task);
        _context.SaveChanges();

        return MapTaskToTaskResponse(task);
    }

    public TaskResponse EditTask(int id, TaskRequestForEdit taskRequestForEdit)
    {
        var existing_task = _context.task.Find(id);
        
        Tasks taskObject = _context.task.Include(t => t.user).FirstOrDefault(t => t.TaskId == id);

        if (existing_task == null)
        {
            return null;
        }

        if (taskRequestForEdit.TaskName != null)
        {
            existing_task.TaskName = taskRequestForEdit.TaskName;
        }

        if (taskRequestForEdit.Status != null)
        {
            existing_task.status = taskRequestForEdit.Status;
        }

        _context.SaveChanges();
        Tasks task = new Tasks
        {
            TaskId = taskObject.TaskId,
            user = taskObject.user,
            TaskName = taskObject.TaskName,
            UserId = taskObject.UserId,
            status = taskObject.status
        };

        return MapTaskToTaskResponse(task);
    }

    public bool DeleteTask(int taskId)
    {
        var taskDelete = _context.task.Find(taskId);

        if (taskDelete == null)
        {
            return false;
        }

        _context.task.Remove(taskDelete);
        _context.SaveChanges();
        return true;
    }

    private TaskResponse MapTaskToTaskResponse(Tasks task)
    {
        return new TaskResponse
        {
            TaskId = task.TaskId,
            TaskName = task.TaskName,
            Status = task.status,
            UserId = task.UserId,
            User = new UserResponse
            {
                Username = task.user?.Username
            }
        };
    }
}