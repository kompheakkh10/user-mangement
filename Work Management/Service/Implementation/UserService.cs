﻿using Microsoft.EntityFrameworkCore;
using Work_Management.Entity;
using Work_Management.Request;
using Work_Management.Response;

namespace Work_Management.Service.Implementation
{
    public class UserService : IUserService
    {
        private readonly SqlDBContext _dbContext;

        public UserService(SqlDBContext sqlDbContext)
        {
            _dbContext = sqlDbContext;
        }

        public async Task<bool> RegisterUserAsync(string username, string password)
        {
            if (_dbContext.user.Any(u => u.Username == username))
            {
                return false;
            }

            string hashPassword = HashPassword(password);

            var newUser = new Users
            {
                Username = username,
                Password = password
            };

            _dbContext.user.Add(newUser);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Authenticate(string username, string password)
        {
            var user = await _dbContext.user
                .Where(u => u.Username == username && u.Password == password)
                .FirstOrDefaultAsync();

            // If user is null, authentication failed
            return user != null;
        }

        public async Task<Response<UserResponse>> GetUser(int id)
        {
            var user = await _dbContext.user.FindAsync(id);
            if (user == null)
            {
                var userResponse = new UserResponse
                {
                    Username = null
                };
                return new Response<UserResponse>
                {
                    Status = 400,
                    Message = "User Not Found!",
                    Data = userResponse
                };
            }
            var userResponseSuccess = new UserResponse
            {
                Username = user.Username
            };
            return new Response<UserResponse>
            {
                Status = 200,
                Message = "User Retrieved Successfully",
                Data = userResponseSuccess
            };
        }

        public async Task<Response<UserResponse>> EditUser(int id, UserRequestWithId userRequest)
        {
            if (id != userRequest.Id)
            {
                return new Response<UserResponse>
                {
                    Status = 400,
                    Message = "cannot find this user",
                    Data = null
                };
            }
            var user = await _dbContext.user.FindAsync(id);
            user.Username = userRequest.Username;
            _dbContext.Entry(user).State = EntityState.Modified;
            
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_dbContext.user.Any(u => u.Id == id))
                {
                    return new Response<UserResponse> { Status = 400, Message = "User not found", Data = null};
                }
                else
                {
                    throw;
                }
            }

            var userResponse = new UserResponse
            {
                Username = userRequest.Username
            };

            return new Response<UserResponse>
            {
                Status = 200,
                Message = "User Update Successfully",
                Data = userResponse
            };
        }

        public async Task<Response<UserResponse>> DeleteUser(int id)
        {
            var getUser = await _dbContext.user.FindAsync(id);
            if (getUser == null)
            {
                return new Response<UserResponse>
                {
                    Status = 400,
                    Message = "User not found!",    
                    Data = null
                };
            }

            _dbContext.user.Remove(getUser);
            await _dbContext.SaveChangesAsync();

            var userResponse = new UserResponse
            {
                Username = getUser.Username
            };

            return new Response<UserResponse>
            {
                Status = 200,
                Message = "User Delete Successfully",
                Data = userResponse
            };
        }

        public async Task<ResponseList<UserResponse>> GetAllUser()
        {
            List<Users> listUser = await _dbContext.user.ToListAsync();
            List<UserResponse> userResponses = new List<UserResponse>();
            foreach (var user in listUser)
            {
                userResponses.Add(new UserResponse
                {
                    Username = user.Username
                });
            }
            return new ResponseList<UserResponse>
            {
                Status = 200,
                Message = "Success",
                Data = userResponses
            };
        }

        private string HashPassword(string password)
        {
            return password;
        }   
    }
}
