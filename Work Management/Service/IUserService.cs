﻿using Work_Management.Request;
using Work_Management.Response;

namespace Work_Management.Service
{
    public interface IUserService
    {
        Task<bool> RegisterUserAsync(string message, string password);
        Task<bool> Authenticate(String username, String password);
        Task<Response<UserResponse>> GetUser(int id);
        Task<Response<UserResponse>> EditUser(int id, UserRequestWithId userRequest);
        Task<Response<UserResponse>> DeleteUser(int id);
        Task<ResponseList<UserResponse>> GetAllUser();
    }
}
