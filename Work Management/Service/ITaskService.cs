﻿
using Work_Management.Entity;
using Work_Management.Request;
using Work_Management.Response;

namespace WorkManagement.Services
{
    public interface ITaskService
    {
        IEnumerable<TaskResponse> GetTasks();
        TaskResponse GetTaskById(int taskId);
        TaskResponse AssignTask(TaskRequest task);
        TaskResponse EditTask(int id, TaskRequestForEdit taskRequestForEdit);
        bool DeleteTask(int taskId);
    }
}