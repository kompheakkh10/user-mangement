﻿using Microsoft.AspNetCore.Mvc;
using Work_Management.Request;
using Work_Management.Response;
using Work_Management.Service;

namespace Work_Management.Controllers
{
    public class AuthenticationController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;

        public AuthenticationController(IConfiguration configuration, IUserService userService)
        {
            _configuration = configuration;
            _userService = userService;
        }

        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromBody] UserRequest userRequest)
        {
            bool authenticate =await _userService.Authenticate(userRequest.Username, userRequest.Password);
            if(authenticate)
            {
                var userResponse = new UserResponse
                {
                    Username = userRequest.Username
                };
                var response = new Response<UserResponse>
                {
                    Status = 200,
                    Message = "login successfully",
                    Data = userResponse
                };
                return Ok(response);
            }
            
            var userResponseFail = new UserResponse
            {
                Username = userRequest.Username
            };
            var responseFail = new Response<UserResponse>
            {
                Status = 401,
                Message = "invalid username or password",
                Data = userResponseFail
            };

            return Unauthorized(responseFail);
        }

        [HttpPost("/create_user")]
        public async Task<IActionResult> Register([FromBody] UserRequest _userRequest)
        {
            if (ModelState.IsValid)
            {
                bool registrationResult =
                    await _userService.RegisterUserAsync(_userRequest.Username, _userRequest.Password);
                if (registrationResult)
                {
                    var userResponse = new UserResponse
                    {
                        Username = _userRequest.Username
                    };
                    
                    var _responseSuccess = new Response<UserResponse>
                    {
                        Status = 201,
                        Message = "user created successfully",
                        Data = userResponse
                    };
                    return Ok(_responseSuccess);
                }
            }
            
            var userResponseFail = new UserResponse
            {
                Username = _userRequest.Username
            };

            var _responseFail = new Response<UserResponse>
            {
                Status = 400,
                Message = "user create fail",
                Data = userResponseFail
            };
            return BadRequest(_responseFail);
        }

        [HttpGet("{id}/get_user")]
        public async Task<IActionResult> GetUser(int id)
        {
            var result = await _userService.GetUser(id);
            return Ok(result);
        }
        
        [HttpPut("{id}/edit")]
        public async Task<IActionResult> EditUser(int id, [FromBody] UserRequestWithId updatedUser)
        {
            var result = await _userService.EditUser(id, updatedUser);
            return Ok(result);
        }
        
        [HttpDelete("{id}/delete")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var result = await _userService.DeleteUser(id);

            if (result.Status == 200)
            {
                return Ok(result);
            }
            else
            {
                return NotFound(result);
            }
        }
        
        [HttpGet("/get_all_user")]
        public async Task<IActionResult> GetAllUsers()
        {
            var result = await _userService.GetAllUser();
            return Ok(result);
        }
    }
}
