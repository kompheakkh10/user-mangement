﻿using Microsoft.AspNetCore.Mvc;
using Work_Management.Request;
using Work_Management.Response;
using WorkManagement.Services;

namespace Work_Management.Controllers
{
    [Route("api/tasks")]
    public class TaskManagementController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TaskManagementController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet("/get_tasks")]
        public  IActionResult GetTasks()
        {
            var tasks =  _taskService.GetTasks(); // Assuming an asynchronous version of GetTasks

            if (tasks == null || !tasks.Any())
            {
                return NotFound("No tasks found");
            }

            return Ok(tasks);
        }
        
        [HttpPost("/create_task")]
        public IActionResult AssignTask([FromBody] TaskRequest taskRequest)
        {
            var task = _taskService.AssignTask(taskRequest);

            // Return the details of the created task
            return Ok(task);
        }
        
        [HttpGet("{taskId}/get_task")]
        public IActionResult GetTaskById(int taskId)
        {
            var task = _taskService.GetTaskById(taskId);

            if (task == null)
            {
                return NotFound($"Task with ID {taskId} not found");
            }

            return Ok(task);
        }
        
        [HttpPut("{taskId}/edit")]
        public IActionResult EditTask(int taskId, [FromBody] TaskRequestForEdit editRequest)
        {
            // Validate input
            if (editRequest == null)
            {
                return BadRequest("Invalid request body");
            }

            TaskResponse editedTask = _taskService.EditTask(taskId, editRequest);
            if (editedTask == null)
            {
                var responseApi = new Response<string>
                {
                   Status = 400,
                   Message = "Task Not Found!",
                   Data = "null"
                };
                return NotFound(responseApi);
            }

            return Ok(editedTask);
        }
        
        [HttpDelete("{taskId}/delete")]
        public IActionResult DeleteTask(int taskId)
        {
            bool deleted = _taskService.DeleteTask(taskId);
            if (!deleted)
            {
                var responseApi = new Response<string>
                {
                    Status = 400,
                    Message = "Task Not Found!",
                    Data = "null"
                };
                return NotFound(responseApi);
            }

            return Ok(new Response<string>
            {
                Status = 200,
                Message = "Success",
                Data = "Delete Successfully"
            });
        }
    }
}