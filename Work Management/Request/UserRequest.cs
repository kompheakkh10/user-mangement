﻿using System.ComponentModel.DataAnnotations;

namespace Work_Management.Request
{
    public class UserRequest
    {
        [Required]
        public string Username {  get; set; }
        [Required]
        public string Password {  get; set; }
    }
}
