﻿namespace Work_Management.Request;

public class UserRequestWithId
{
    public int Id { get; set; }
    public string Username { get; set; }
}