﻿namespace Work_Management.Request;

public class TaskRequestForEdit
{
    public string TaskName { get; set; }
    public string Status { get; set; }
}