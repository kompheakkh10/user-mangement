﻿using System.ComponentModel.DataAnnotations;

namespace Work_Management.Request;

public class TaskRequest
{
    [Required]
    public string TaskName { get; set; }
    [Required]
    public string Status { get; set; }
    [Required]
    public int UserId { get; set; }
}