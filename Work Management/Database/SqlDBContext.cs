﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Work_Management.Entity;

namespace Work_Management;

public class SqlDBContext : DbContext
{
    public SqlDBContext(DbContextOptions<SqlDBContext> options) : base(options)
    {
    }
    
    public DbSet<Users> user { get; set; }
    
    public DbSet<Tasks> task { get; set; }
/*
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new UsersEntityTypeConfig());
        SeedProductData(modelBuilder.Entity<Users>());
    }
    
    private void SeedProductData(EntityTypeBuilder<Users> entityTypeBuilder)
    {
        var reqs = new List<Users>()
        {
            new()
            {
                Id = 1,
                Username = "Kompheak",
                Password = "Kompheak@1234"
            }
        };
        entityTypeBuilder.HasData(reqs);
    }*/
}