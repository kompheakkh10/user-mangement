﻿/*using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Work_Management.Entity;

namespace Work_Management
{
    public class UsersEntityTypeConfig : IEntityTypeConfiguration<Users>
    {
        public void Configure(EntityTypeBuilder<Users> builder)
        {
            builder.ToTable("users");
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Id).IsUnique(true);

            builder.Property(x => x.Id)
                .IsRequired(true)
                .HasColumnName(nameof(Users.Id))
                .HasColumnType("int");
            
            builder.Property(x => x.Username)
                .IsRequired(true)
                .HasColumnName(nameof(Users.Username))
                .HasColumnType("varchar")
                .HasMaxLength(36);
            
            builder.Property(x => x.Password)
                .IsRequired(true)
                .HasColumnName(nameof(Users.Password))
                .HasColumnType("varchar")
                .HasMaxLength(36);
        }
    }
}
*/