﻿using Microsoft.EntityFrameworkCore;
using Work_Management.Entity;

namespace Work_Management;

public class SeedData
{
    public static void PrepPopulation(IApplicationBuilder app)
    {
        using var serviceScope = app.ApplicationServices.CreateScope();
        SeedDatas(serviceScope.ServiceProvider.GetService<SqlDBContext>());
    }

    private static void SeedDatas(SqlDBContext sqlDbContext)
    {
        sqlDbContext.Database.Migrate();
        var reqs = new List<Users>()
        {
            new()
            {
                Username = "Kompheak",
                Password = "Kompheak@1234"
            }
        };

        if (!sqlDbContext.user.Any())
        {
            sqlDbContext.AddRange(reqs);
        }

        sqlDbContext.SaveChanges();
    }

}