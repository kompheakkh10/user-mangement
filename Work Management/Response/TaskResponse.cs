﻿namespace Work_Management.Response;

public class TaskResponse
{
    public int TaskId { get; set; }
    public string TaskName { get; set; }
    public string Status { get; set; }
    public int UserId { get; set; }
    public UserResponse User { get; set; }
}