﻿using System.Text.Json.Serialization;

namespace Work_Management.Response;

public class ResponseList<T>
{
    public int Status { get; set; }
    public string Message { get; set; }
    [JsonIgnore]
    public List<T> Data { get; set; }
}