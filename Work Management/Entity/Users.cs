﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Work_Management.Entity
{
    [Table("users", Schema = "public")]
    public class Users
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Column(TypeName = "text")]
        public string Username { get; set; }
        
        [Column(TypeName = "text")]
        public string Password { get; set; }
    }
}
