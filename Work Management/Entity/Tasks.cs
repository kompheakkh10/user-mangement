﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Work_Management.Entity;

[Table("tasks", Schema = "public")]
public class Tasks
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int TaskId { get; set; }
    
    [Column(TypeName = "text")]
    public string? TaskName { get; set; }
    
    [Column(TypeName = "text")]
    public string status { get; set; }
    
    public int UserId { get; set; }
    
    [ForeignKey("UserId")]
    public Users user { get; set; }
    
}